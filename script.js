document.querySelector('.data-form').addEventListener('submit', (e) => {
    e.preventDefault();
});



function getCepInfo(e) {
    // Pega valor do cep do input
    const cep = document.querySelector('.cep').value;

    console.log(cep);

    // Faz a request
    fetch(`https://viacep.com.br/ws/${cep}/json`)
        .then(response => {
            return response.json();
        })
        .then(data => {
            // Exibe informação do CEP
            if (data.erro) {
                alert('CEP INVÁLIDO')
            } else {
                document.querySelector('.address').value = `${data.logradouro}`;
                document.querySelector('.address-city').value = `${data.localidade}`;
                document.querySelector('.address-state').value = `${data.uf}`;
            }
        });
}

window.addEventListener('load', function () {
    new Glider(document.querySelector('.glider'), {
        slidesToShow: 1,
        slidesToScroll: 1,
        draggable: true,
        itemWidth: 150,
        rewind: true,
        dots: '.dots',
        arrows: {
            prev: '.glider-prev',
            next: '.glider-next'
        },
        responsive: [
            {
                // screens greater than >= 1024px
                breakpoint: 1250,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    itemWidth: 150,
                    duration: 0.25
                }
            },
            {
                // screens greater than >= 1024px
                breakpoint: 750,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    itemWidth: 150,
                    duration: 0.25
                }
            },
        ]
    });
});

const menuResponsivo = document.querySelector('.responsive-header-button');
menuResponsivo.addEventListener('click', function (e) {
    this.classList.toggle('open');
    e.preventDefault();
})


/* Footer responsivo sobre */
const aboutFooter = document.querySelector('.about-responsive-footer');
aboutFooter.addEventListener('click', function (e) {
    this.classList.toggle('open');
    console.log(aboutFooter);
    e.preventDefault();
})

const closeButton = document.querySelector('.about-close-button');
closeButton.addEventListener('click', function (e) {
    aboutFooter.classList.toggle('open');
    console.log(closeButton);
    e.preventDefault();
})

/* Footer responsivo Produtos*/
const productsFooter = document.querySelector('.products-responsive-footer');
productsFooter.addEventListener('click', function (e) {
    this.classList.toggle('open');
    console.log(productsFooter);
    e.preventDefault();
})

const closeButton2 = document.querySelector('.products-close-button');
closeButton2.addEventListener('click', function (e) {
    productsFooter.classList.toggle('open');
    e.preventDefault();
})


/* Footer responsivo Suporte*/
const suportFooter = document.querySelector('.suport-responsive-footer');
suportFooter.addEventListener('click', function (e) {
    this.classList.toggle('open');
    console.log(suportFooter);
    e.preventDefault();
})

const closeButton3 = document.querySelector('.suport-close-button');
closeButton3.addEventListener('click', function (e) {
    suportFooter.classList.toggle('open');
    e.preventDefault();
})

/* Footer responsivo Atendimento*/
const attendanceFooter = document.querySelector('.attendance-responsive-footer');
attendanceFooter.addEventListener('click', function (e) {
    this.classList.toggle('open');
    console.log(attendanceFooter);
    e.preventDefault();
})

const closeButton4 = document.querySelector('.attendance-close-button');
closeButton4.addEventListener('click', function (e) {
    attendanceFooter.classList.toggle('open');
    e.preventDefault();
})

